# ESLint and Prettier: The ultimate duo for writing bug free and beautifully formatted code in React/React-Native

![Beauty](./titleImage.jpg)

Code structure and readability is as important as code logic, and in larger codebases it should be prioritize earlier and higher else it would be a huge nightmare to maintain large codebases. The code should be structured well and readable for other team members working on the same codebase. The team should follow consistent coding patterns so that it's easy for anyone to dive into the code and get started working.

In this article we will get the linting and formartting React-Native code in a consistent style in VSCode to a level so that maintaining large codebases wouldn't be a nightmare anymore. Writing clean code consists of following two rules:

1. **Code-quality rules:**  These rules allows us to write code that shouldn't inherit bugs. We will use **[ESLint](https://eslint.org/)** to adhere to these rule. ESLint will warn us whenever we deviate from good code quality. While writing React-Native we need to consider code-quality rules for *Javascript* and *React* domains, like:

   * **Javascript:** Some basic JS rules are, *we shouldn't declare variables which aren't used anywhere in the code*, *we shouldn't do unnecessary function bindings* etc. These rules reduces the probability of bugs inherited due to bad JS code.

   * **React:** Alongside Javascript, we should also adhere to React specific rules that helps us to follow React's best practices. Some React specific rules are, *we should declare PropTypes for every component*, *we should avoid string references* etc.

2. **Formatting rules:** These rules allow us to write beautiful looking code which is properly indented, follows proper spacing and is easily readable. We will be using **[Prettier](https://prettier.io/)** to follow these rules.

Enough of theory, now let's get started in setting up ESLint and Prettier in VSCode:

### 1. Setting up **ESLint**:
   * First we need to install the linter for ESLint in VSCode that will show the warnings and error as we type code. To install, open the Extension View in the Activity bar in the side of VSCode or by using the command `⇧⌘X`. Now seearch for **[this ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)** and install it.

   * Now we need to install ESLint as devDependency using npm as:  
        ```bash
        npm install eslint --save-dev
        ```
   
   * Now we will add linting rules to our eslint config, our code will adhere to these rules. We will use the very popular [Airbnb's Javascript styleguid](https://github.com/airbnb/javascript) and install it as follows:  
       * First run this command  
            ```bash
            npm info "eslint-config-airbnb@latest" peerDependencies
            ```

       * The above command will output the correct versions of all the peer dependencies of `eslint-config-airbnb`. Then run the following command to install `eslint-config-airbnb` and all the peer dependencies:  
            ```bash
            npm install --save-dev eslint-config-airbnb eslint-plugin-jsx-a11y@^#.#.# eslint-plugin-import@^#.#.# eslint-plugin-react@^#.#.#
            ```

        If necessary, official guidelines can be found here: https://www.npmjs.com/package/eslint-config-airbnb

   * Now we have to install a [parser for ESLint, babel-eslint](https://github.com/babel/babel-eslint). We need this parser to parse ES7 syntax like Javascript classes which are not yet supported by the default ESLint parser. We use npm to install it:  
        ```bash
        npm install babel-eslint --save-dev
        ```

   * Our ESLint setup is complete except if there isn't any `.eslintrc` file in the root folder of our project. If `.eslintrc` is not there in the root folder than we can create simply like any other file or by running `eslint --init` in our project's root. We need to further configure our `.eslintrc` as follows:  
        ```json
                {
                    "extends": "airbnb",
                    "parser": "babel-eslint",
                    "ecmaFeatures": {
                        "classes": true
                    }
                }
        ```
   This is the minimum configuration required and pretty much self explanatory. We can [further configure the `.eslintrc`](https://eslint.org/docs/user-guide/configuring) but that will be beyond the scope of this article.

   Finally, we are done with our ESLint setup. You can now create a React/React-Native project and add a `.eslintrc` file to the project's root folder with the above mentioned ESLint configuration and be ready for the VScode to moan at you.

![Without ESLint](./withoutESLint.png) *code snippet without using ESLint*

![With ESLint](./withESLint.png) *code snippet with ESLint linting the code*

At this stage ESLint is handling our code-quality and formatting (the two rules mentioned above). Our ESLint config contains a rule which defines the width of a line i.e maximum number of characters that can be contained in a line. If the number of characters exceeds the width of the line, than ESLint will warn us that the rule is not followed. To fix the warning/error we have to fix the code manually. It's very time consuming to consider code formatting and deviates our mind from coding the main logic. Well this difficulty used to be the thing of past, enter **_Prettier_**. 

**Prettier** is an opinionated code formatter which enforces a consistent code style, It will make you code look beautiful and readable. What we will do is, offloading the code formatting responsibilities from ESLint and put it on prettier's shoulders. 

### 2. Setting up **Prettier**:
   * First hit up the VSCode Marketplace and install the **[Prettier Extension for VSCode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)**.

   * After installing the extension, hit `⌘,` and edit the following line in [VSCode Workspace settings](https://code.visualstudio.com/docs/getstarted/settings):  
        ```bash 
            "editor.formatOnSave": true,
        ```  
   This will enable code formatting on saving the file.

   * Second, Since want Prettier to handle the formatting duties, we can install [eslint-config-prettier](https://github.com/prettier/eslint-config-prettier) and extend it from our `.eslintrc`. We will install it using npm as follows:  
        ```bash
        npm install --save-dev eslint-config-prettier
        ```  
        Basically it turns off the ESLint formatting rules which are conflicting with Prettier's formatting rules. For example: Airbnb's ESLint config prefers singlequote for strings whereas prettier prefers doublequote. Thus we can override the ESLint's formating rules and replace them by Prettier's formatting rules. To turn on this behavior we need to extend "prettier" in `.eslintrc` as follows:  

        ```json
                {
                    "extends": ["airbnb", "prettier"],
                    "parser": "babel-eslint",
                    "ecmaFeatures": {
                    "classes": true
                    }
                }       
        ```  

        **Note**: "prettier" should be added in the last, so that it can override other configs.  
        Now, whenever there is a conflicting "formatting rule" between ESLint and Prettier, Prettier's rule will take precedence. 
       
        Here is a demo of the singlequote/doublequote example mentioned above. On saving the file, singlequote(ESLint's rule) are overriden by doublequote(Prettier's rule): 

        ![Prettier's rule takes precedence over ESLint's, No warings shown](./prettierEslint.gif)

    * There's still an issue. We want ESLint to show us errors when prettier rules are not followed. For example, in the above case linter(ESLint) should warn us that we aren't using doublequote as specified by Prettier. We can solve this issue by running Prettier as an ESLint rule so that ESLint will moan at us whenever we don't follow Prettier's rule. We will use  [eslint-plugin-prettier](https://github.com/prettier/eslint-plugin-prettier) to report us whenever we don't follow Prettier's rule. We will install it using npm:  
       ```bash
       npm install --save-dev prettier eslint-plugin-prettier
       ``` 

       Now we need to add this plugin to our `.eslintrc`, and now our `.eslintrc` will look as follows: 
       ```json
       {
            "extends": ["airbnb", "prettier"],
            "parser": "babel-eslint",
            "ecmaFeatures": {
                "classes": true
            },
            "plugins": ["prettier"],
            "rules": {
                "prettier/prettier": "error"
            }
        }
       ```
        ESLint and Prettier have found the harmony. So if we unfollow any Prettier rule, ESLint will scold us. Here is a demo, ESLint is warning us that we are using singlequote(ESLint rule) instead of doublequote(Prettier's rule, Prettier's "formatting rules" takes precedence over ESLint's.):  

        ![Prettier's rule takes precedence over ESLint's, No warings shown](./prettierEslint1.gif)

## Conclusion
Finally, we have integrated ESLint and Prettier in our React/React-Native project. We will now concentrate more on our code logic, ESLint+Prettier will help us in identifying bugs and beautifying the code.
    
   